import scipy.io as sio
import numpy as np
import heapq
import matplotlib.pyplot as plt
from sklearn import manifold
#from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import coverage_error
from sklearn.metrics import hamming_loss
from sklearn.cross_validation import KFold
from sklearn.linear_model import Ridge

Bibtex = sio.loadmat("bibtex.mat")
#print type(Bibtex)
#print Bibtex
X_tr = Bibtex["X_tr"].todense().tolist()
X_te = Bibtex["X_te"].todense().tolist()
Y_tr = Bibtex["Y_tr"].todense().tolist()
Y_te = Bibtex["Y_te"].todense().tolist()
#print X_tr.shape, X_te.shape, Y_tr.shape, Y_te.shape

for r in Y_tr[0: 5]:
    print r
#plt.matshow(Y_tr[0:150])
#plt.show()


#X, Y = [], []
'''
for i in range(25, 120, 10):
    print i
    #Iso = manifold.Isomap(n_components = i)
    Pca = PCA(n_components = i)
    #getData("bibtex.mat")
    Y_tr_trans = Pca.fit_transform(Y_tr)
    #X.append(Iso.get_params(deep = True)['n_components']), Y.append(Iso.reconstruction_error())
    print Pca.get_params(deep = True)['n_components'], Pca.get_precision()
'''
#plt.plot(X, Y)
#plt.savefig("reconstruction_error_zoomed.png")
#lab

#print Y_tr[0:5]
Iso = manifold.Isomap(n_neighbors = 50, n_components = 30)
Y_tr_trans = Iso.fit_transform(Y_tr)

#LR = LinearRegression()
#LR.fit(X_tr, Y_tr_trans)

#Trying Ridge
RR = Ridge(alpha = 5)
RR.fit(X_tr, Y_tr_trans)

Y_te_trans_pred = RR.predict(X_te)
print "k  \t  Coverage error  \t  ones-pre      \t  twos-pre       \t  threes-pre"
print "------------------------------------------------------------------------------------------------"
for k in range(50, 200, 20):
    Knn = KNeighborsClassifier(n_neighbors = k)
    Knn.fit(Y_tr_trans, range(0, len(Y_tr_trans)))
    (_, index) = Knn.kneighbors(Y_te_trans_pred)
    index = index.tolist();
    result = []
    for inds in (index):
        res = np.array([0.0]*159)
        for ind in inds:
            res += (Y_tr[ind])
        result.append(res)
    predi = []
    for r in result:
        q = heapq.nlargest(3, [(v, i) for (i, v) in enumerate(r)])
        predi.append(q)
    sco1, sco2, sco3 = 0.0, 0.0, 0.0
    for i, y in enumerate(Y_te):
        im1 = predi[i][0][1]
        im2 = predi[i][1][1]
        im3 = predi[i][2][1]
        if(y[im1]):
            sco1+=1.0
        if(y[im2]):
            sco2 += 1
        if(y[im3]):
            sco3 += 1
    hloss = 0
#    for i in range(0, len(res)):
#        hloss += hamming_loss(Y_te[i], res[i])
#    print "Hammings loss:", hloss/2515
    oneP = sco1/len(Y_te)
    twoP = sco2/len(Y_te)
    thrP = sco3/len(Y_te)
    print k," \t ", coverage_error(Y_te, result)," \t ", oneP," \t ", twoP," \t ", thrP
