import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
from sklearn import manifold
from sklearn.linear_model import LinearRegression

Bibtex = sio.loadmat("bibtex.mat")
#print type(Bibtex)
#print Bibtex
X_tr = Bibtex["X_tr"].todense()
X_te = Bibtex["X_te"].todense()
Y_tr = Bibtex["Y_tr"].todense()
Y_te = Bibtex["Y_te"].todense()
#print X_tr.shape, X_te.shape, Y_tr.shape, Y_te.shape

#plt.matshow(Y_tr[0:150])
#plt.show()

#plt.plot(X, Y)
#plt.savefig("reconstruction_error_zoomed.png")
#lab

Iso = manifold.Isomap(n_components = 45)
X_tr_trans = Iso.fit_transform(X_tr)
LR = LinearRegression()

