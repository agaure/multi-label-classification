import scipy.io as sio
import numpy as np
import heapq
import matplotlib.pyplot as plt
from sklearn import manifold
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import coverage_error
from sklearn.metrics import hamming_loss
from yeastRip import getDat
#Scan data------------------------------------
Bibtex = sio.loadmat("bibtex.mat")
X_tr = Bibtex["X_tr"].todense()
X_te = Bibtex["X_te"].todense()
Y_tr = Bibtex["Y_tr"].todense()
Y_te = Bibtex["Y_te"].todense()
#---------------------------------------------
#Scan Data Yeast------------------------------
'''
X_tr, Y_tr = getDat("yeast-10-1tra.dat")
X_te, Y_te = getDat("yeast-10-1tst.dat")
print X_tr.shape, X_te.shape, Y_tr.shape, Y_te.shape
#---------------------------------------------
'''
Iso = manifold.Isomap(n_components = 45)
Y_tr_trans = Iso.fit_transform(Y_tr)
LR = LinearRegression()
LR.fit(X_tr, Y_tr_trans)
Y_te_trans_pred = LR.predict(X_te)

#Parameters-----------------------------------
k = 75
#---------------------------------------------

#Computing Prior P(H{l,1}), P(H{l,0}) --------
(m, L) = Y_tr.shape
sum_label_array = np.sum(Y_tr, axis = 0)
sum_label_array = np.array(sum_label_array.tolist()[0])
P_Hl1 = sum_label_array / m
P_Hl0 = 1 - P_Hl1
#---------------------------------------------

#Computing the Posterior Probability P(E{l,j}|H{l,i})--------------
P_E_Hi1 = np.matrix([[0.0]*(k+1)]*L)
P_E_Hi0 = np.matrix([[0.0]*(k+1)]*L)
KNN = KNeighborsClassifier(n_neighbors = k)
KNN.fit(Y_tr_trans, Y_tr)
N = KNN.kneighbors(Y_tr_trans, return_distance = False)
C = np.matrix([[0.0]*L]*m)
for l in range(0, L):
    c1 = np.array([0.0]*(k+1))
    c0 = np.array([0.0]*(k+1))
    for xi, N_xi in enumerate(N):
        delta = 0.0
        for a in N_xi:
            delta += Y_tr[a, l]
        C[xi, l] = delta
        if(Y_tr[xi, l]):
            c1[delta] += 1.0
        else:
            c0[delta] += 1.0
    cp = c1.sum()
    cn = c0.sum()
    for j in range(0, k+1):
        P_E_Hi1[l, j] = c1[j] / cp
        P_E_Hi0[l, j] = c0[j] / cn
#----------------------------------------------

#Computing Y_te and Rt-------------------------
N = KNN.kneighbors(Y_te_trans_pred, return_distance = False)
(m, L) = Y_te.shape
C = np.matrix([[0.0]*L]*m)
Y_te_res = np.matrix([[0.0]*L]*m)
rank = np.matrix([[0.0]*L]*m)
for l in range(0, L):
    for xi, N_xi in enumerate(N):
        delta = 0.0
        for a in N_xi:
            delta += Y_tr[a, l]
        C[xi, l] = delta
        if(P_Hl0[l]*P_E_Hi0[l, delta] < P_Hl1[l]*P_E_Hi1[l, delta]):
            Y_te_res[xi, l] = 1.0
        else:
            Y_te_res[xi, l] = 0.0
        numer = P_Hl1[l] * P_E_Hi1[l, delta]
        denom = P_Hl0[l]*P_E_Hi0[l, delta] + P_Hl1[l]*P_E_Hi1[l, delta]
        if(denom != 0.0):
            rank[xi, l] = numer/denom
#----------------------------------------------------------------------------------

predi = []
rank = rank.tolist()
for r in rank:
    q = heapq.nlargest(3, [(v, i) for (i, v) in enumerate(r)])
    predi.append(q)

sco1, sco2, sco3 = 0.0, 0.0, 0.0
Y_te = Y_te.tolist()
Y_te_res = Y_te_res.tolist()
for i, y in enumerate(Y_te):
    im1 = predi[i][0][1]
    im2 = predi[i][1][1]
    im3 = predi[i][2][1]
    if(y[im1]):
        sco1 += 1.0
    if(y[im2]):
        sco2 += 1.0
    if(y[im3]):
        sco3 += 1.0

hloss = 0.0
for i in range(0, len(Y_te_res)):
    hloss += hamming_loss(Y_te[i], Y_te_res[i])

print "Coverage error:",coverage_error(Y_te, rank), " ones-pre:", sco1/len(Y_te), " two-pre:", sco2/len(Y_te), " three-pre:", sco3/len(Y_te)
#print zip(Y_te[0], rank[0])
#print zip(P_Hl0.tolist(), P_Hl1.tolist())
#print zip(P_E_Hi0.tolist(), P_E_Hi1.tolist())
print "Hammings loss:", hloss/len(Y_te)
#print  Y_te_res, rank
