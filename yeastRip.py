import numpy as np

def getDat(path):
    f = open(path, 'r')
    lines = f.readlines()
    X_tr, Y_tr = [], []
    for line in lines:
        rawDat = (line.strip("\r\n").split(','))
        rawDat = [float(x) for x in rawDat]
        X_tr.append(rawDat[0:103]), Y_tr.append(rawDat[103: ])
    return np.matrix(X_tr), np.matrix(Y_tr)

